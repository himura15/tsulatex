rm *.gz
rm *.log
rm *.aux
rm *.bbl
rm *.out
rm *.toc
rm *.blg
rm *.pdf
rm *.fls
rm *.fdb_*

pdflatex 10_minimal.tex
pdflatex 20_main-1.tex

pdflatex 30_main-2.tex
pdflatex 30_main-2.tex

pdflatex 40_main-3.tex
pdflatex 40_main-3.tex

pdflatex 50_main-4.tex
bibtex 50_main-4
pdflatex 50_main-4.tex
pdflatex 50_main-4.tex

pdflatex 60_main-5.tex
pdflatex 60_main-5.tex

rm *.gz
rm *.log
rm *.aux
rm *.bbl
rm *.out
rm *.toc
rm *.blg
pause
